* root :elfeed:
** Programming :programming:
*** [[https://this-week-in-rust.org/rss.xml][This Week in Rust]] :rust:
*** [[https://fasterthanli.me/index.xml][Faster Than Lime]] :rust:
*** [[https://nim-lang.org/feed.xml][Nim Blog]] :nim:
*** [[http://feeds.feedburner.com/codinghorror][Coding Horror]] :blog:
*** [[https://zigmonthly.org/index.xml][Zig Monthly]] :zig:
*** [[https://githubengineering.com/][Github Engineering]]
*** [[https://ysantos.com/rss][Y. D. Santos’ Blog.]] :rust:
*** [[https://clojure.org/feed.xml][Clojure News]] :clojure:
*** [[https://elixirstatus.com/rss][Elixir Status]] :elixir:
*** [[https://kill-the-newsletter.com/feeds/g8m9noc5c5v1pjzi.xml][Elixir Weekly]] :elixir:
*** [[https://dashbit.co/feed][Dashbit]] :elixir:
*** [[https://nitter.net/hashrockettil/rss][HashRocket TIL]]
*** [[https://kill-the-newsletter.com/feeds/zm5flavpj8mb8tsz.xml][Hafta Sonu Okumaları]]
*** [[https://kill-the-newsletter.com/feeds/uys8kupivg5lamp6.xml][EmbedSys]]
*** [[https://sin-ack.github.io/index.xml][SinAck]] :zig:
*** [[https://thisweekinzig.mataroa.blog/rss/][This Week In Zig]] :zig:

** Emacs :emacs:
*** [[https://aliquote.org/index.xml][aliquote]] :blog:
*** [[https://blog.tecosaur.com/tmio/rss.xml][This Month In Org]] :org:
*** [[https://sachachua.com/blog/feed/][Sacha Chua]] :blog:

** Security
*** [[https://krebsonsecurity.com/feed/][Krebs On Security]]
*** [[https://erfur.github.io/feed.xml][Erfur]]
*** [[https://nitter.foss.wtf/__TTMO__/rss][TTMO]]
